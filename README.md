### Welcome to my Portfolio!
[![Author: Paulo Canilho](https://img.shields.io/badge/Author/Website-Paulo%20Canilho-yellowgreen.svg)](http://www.pcanilho.com)
[![LinkedIn: www.pcanilho.com](https://img.shields.io/badge/LinkedIn-visit%20profile-yellow.svg)](https://www.linkedin.com/in/paulo-alexandre-pereira-martins-canilho-41480b109/)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Release Version](https://img.shields.io/badge/Version-v1.0.1-green.svg)](https://gitlab.com/pcanilho/portfolio)
[![Open Issues](https://img.shields.io/badge/Issues-0%20open-lightgrey.svg)](https://gitlab.com/pcanilho/portfolio/issues)

---
##### In this table you will be able to see the projects and/or publications that I am currently working/worked on:
<b>Domain</b> |  <b>Git URL</b>
:--- | :---:
<b><i>`MSc dissertation project (08/2017)`</i></b>     |    <b>[link](https://gitlab.com/pcanilho/portfolio/tree/education)</b>   
    
---
##### In this table you will be able to see the range of programming languages & technologies that I am currently working:
</b>  |   |  <b>Git URL</b>
:---: | :--- | :---:
<a href="https://gitlab.com/pcanilho/portfolio/tree/GoLang"><img alt="Image of GoLang" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/23/Go_Logo_Aqua.svg/1024px-Go_Logo_Aqua.svg.png" height="50" width="175"></a> |    <b><i>`GoLang`</i></b> 	| 	<b>[link](https://gitlab.com/pcanilho/portfolio/tree/Golang)</b>
<a href="https://gitlab.com/pcanilho/portfolio/tree/AdaSpark-2012"><img alt="Image of Ada Spark" src="https://upload.wikimedia.org/wikipedia/commons/c/cd/AdaCore_%28logo%29.jpg" height="50" width="175"></a> |    <b><i>`Ada Spark (2012+)`</i></b> 	| 	<b>[link](https://gitlab.com/pcanilho/portfolio/tree/AdaSpark-2012)</b>
<a href="https://gitlab.com/pcanilho/portfolio/tree/Android"><img alt="Image of Android" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Android_logo_green.svg/2000px-Android_logo_green.svg.png" height="50" width="175"></a> |    <b><i>`Android`</i></b> 	|	<b>[link](https://gitlab.com/pcanilho/portfolio/tree/Android)</b>
<a href="https://gitlab.com/pcanilho/portfolio/tree/Java"><img alt="Image of Java" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Oracle_logo.svg/2000px-Oracle_logo.svg.png" height="50" width="175"></a> |    <b><i>`Java (includes SQL integrations, e.g. AWS Redshift)`</i></b> 	|	<b>[link](https://gitlab.com/pcanilho/portfolio/tree/Java)</b>
<a href="https://gitlab.com/pcanilho/portfolio/tree/DotNet"><img alt="Image of DotNet" src="http://1.bp.blogspot.com/-COH3Bsi3waI/Vhkq1SbDSLI/AAAAAAAAAbI/_E4-RIKmHeM/s400/netframework-logo.jpg" height="50" width="175"></a> |    <b><i>`C#`</i></b> 	|	<b>[link](https://gitlab.com/pcanilho/portfolio/tree/DotNet)</b>
<a href="https://gitlab.com/pcanilho/portfolio/tree/NodeJS"><img alt="Image of Node.JS" src="https://upload.wikimedia.org/wikipedia/commons/7/7e/Node.js_logo_2015.svg" height="50" width="175"></a> |    <b><i>`Node.JS`</i></b> 	|	<b>[link](https://gitlab.com/pcanilho/portfolio/tree/NodeJS)</b>
<a href="https://gitlab.com/pcanilho/portfolio/tree/Python"><img alt="Image of Python" src="https://c1.staticflickr.com/9/8123/8679381967_75cee4e0e9_z.jpg" height="50" width="175"></a> |    <b><i>`Python`</i></b> 	|	<b>[link](https://gitlab.com/pcanilho/portfolio/tree/Python)</b>
<a href="https://gitlab.com/pcanilho/portfolio/tree/SQL"><img alt="Image of SQL" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Mariadb-seal-browntext.svg/2000px-Mariadb-seal-browntext.svg.png" height="50" width="175"></a> |    <b><i>`SQL (includes MariaDB and MySQL)`</i></b> 	|	<b>[link](https://gitlab.com/pcanilho/portfolio/tree/SQL)</b>
<a href="https://gitlab.com/pcanilho/portfolio/tree/GameEngines"><img alt="Image of Game Engines" src="https://gitlab.com/pcanilho/portfolio/raw/GameEngines/engine_lwjgl3/imgs/oc_emblem_badge.png" height="50" width="50"></a> | <b><i>`Direct3D/DirectX 11 & Vulkan & OpenGL (C++11 & JDK8)`</i></b> 	|	<b>[link](https://gitlab.com/pcanilho/portfolio/tree/GameEngines)</b>
</b>    |    <b><i>`Other`</i></b>	|	<b>[link](https://gitlab.com/pcanilho/portfolio/tree/Other)</b>
 


